import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import TodoList from "./TodoList";
import 'font-awesome/css/font-awesome.min.css';

var destination = document.querySelector("#container");

ReactDOM.render(
    <div>
        <p>REACT TODO LIST</p>
        <TodoList/>
    </div>,
    destination
);